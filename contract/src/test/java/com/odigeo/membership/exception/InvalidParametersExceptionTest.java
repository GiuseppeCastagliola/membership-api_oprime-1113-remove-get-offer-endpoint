package com.odigeo.membership.exception;

import com.odigeo.membership.exception.InvalidParametersException.InvalidParametersReason;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

/**
 * Created by roc.arajol on 29-Jan-18.
 */
public class InvalidParametersExceptionTest {

    public static final String MESSAGE = "MESSAGE";

    @Test
    public void testInvalidParametersException() {
        Throwable cause = new Throwable("CAUSE");
        InvalidParametersException invalidParametersException = new InvalidParametersException(
                InvalidParametersReason.BAD_REQUEST, MESSAGE, cause);

        assertEquals(invalidParametersException.getMessage(), MESSAGE);
        assertEquals(invalidParametersException.getCause(), cause);
    }

    @Test
    public void testInvalidParametersExceptionEmptyCause() {
        InvalidParametersException invalidParametersException = new InvalidParametersException(InvalidParametersReason.BAD_REQUEST, MESSAGE);

        assertEquals(invalidParametersException.getMessage(), MESSAGE);
        assertNull(invalidParametersException.getCause());
    }
}
