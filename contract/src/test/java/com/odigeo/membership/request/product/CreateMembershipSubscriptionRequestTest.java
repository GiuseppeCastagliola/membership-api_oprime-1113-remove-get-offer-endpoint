package com.odigeo.membership.request.product;

import com.odigeo.utils.BeanTest;

import java.math.BigDecimal;

public class CreateMembershipSubscriptionRequestTest extends BeanTest<CreateMembershipSubscriptionRequest> {

    @Override
    protected CreateMembershipSubscriptionRequest getBean() {
        CreateMembershipSubscriptionRequest membershipSubscriptionRequest = new CreateMembershipSubscriptionRequest();
        membershipSubscriptionRequest.setBalance(BigDecimal.ZERO);
        return membershipSubscriptionRequest;
    }

    @Override
    protected boolean checkEquals() {
        return false;
    }

    @Override
    protected boolean checkHashCode() {
        return false;
    }
}
