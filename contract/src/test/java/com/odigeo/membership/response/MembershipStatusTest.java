package com.odigeo.membership.response;

import com.odigeo.utils.BeanTest;

public class MembershipStatusTest extends BeanTest<MembershipStatus> {

    private static final String STATUS = "status";

    @Override
    protected MembershipStatus getBean() {
        return new MembershipStatus(STATUS).setStatus(STATUS);
    }

    @Override
    protected boolean checkEquals() {
        return false;
    }

    @Override
    protected boolean checkHashCode() {
        return false;
    }
}
