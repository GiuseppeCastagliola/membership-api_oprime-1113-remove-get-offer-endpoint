package com.odigeo.membership.response;

import com.odigeo.utils.BeanTest;

import java.util.Date;

public class BlackListedPaymentMethodTest extends BeanTest<BlackListedPaymentMethod> {

    @Override
    protected BlackListedPaymentMethod getBean() {
        BlackListedPaymentMethod blackListedPaymentMethod = new BlackListedPaymentMethod();
        blackListedPaymentMethod.setId(0L);
        blackListedPaymentMethod.setTimestamp(new Date());
        return blackListedPaymentMethod;
    }

    @Override
    protected boolean checkEquals() {
        return false;
    }

    @Override
    protected boolean checkHashCode() {
        return false;
    }
}
