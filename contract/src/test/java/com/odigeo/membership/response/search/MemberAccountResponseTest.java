package com.odigeo.membership.response.search;

import com.odigeo.utils.BeanTest;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;

public class MemberAccountResponseTest extends BeanTest<MemberAccountResponse> {
    private static final String NAME = "name";
    private static final String LAST_NAME = "lastName";
    private static final Long USER_ID = 123L;
    private static final long MEMBER_ACCOUNT_ID = 123L;

    @Override
    protected MemberAccountResponse getBean() {
        MemberAccountResponse memberAccountResponse = new MemberAccountResponse();
        memberAccountResponse.setId(MEMBER_ACCOUNT_ID);
        memberAccountResponse.setLastNames(LAST_NAME);
        memberAccountResponse.setName(NAME);
        memberAccountResponse.setUserId(USER_ID);
        return memberAccountResponse;
    }

    @Override
    protected boolean checkEquals() {
        return false;
    }

    @Override
    protected boolean checkHashCode() {
        return true;
    }

    @Test
    public void testEquals() {
        List<MembershipResponse> membershipResponseList = Collections.singletonList(new MembershipResponse());
        EqualsVerifier.forClass(MemberAccountResponse.class)
                .withPrefabValues(List.class, membershipResponseList, Collections.emptyList())
                .suppress(Warning.NONFINAL_FIELDS)
                .usingGetClass()
                .verify();
    }
}
