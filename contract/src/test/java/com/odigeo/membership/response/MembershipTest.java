package com.odigeo.membership.response;

import com.odigeo.utils.BeanTest;

import java.math.BigDecimal;

public class MembershipTest extends BeanTest<Membership> {

    @Override
    protected Membership getBean() {
        Membership membership = new Membership();
        membership.setBalance(BigDecimal.ZERO);
        return membership;
    }

    @Override
    protected boolean checkEquals() {
        return false;
    }

    @Override
    protected boolean checkHashCode() {
        return false;
    }
}
