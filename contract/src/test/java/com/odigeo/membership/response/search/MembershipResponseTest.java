package com.odigeo.membership.response.search;

import com.odigeo.utils.BeanTest;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;

public class MembershipResponseTest extends BeanTest<MembershipResponse> {
    private static final String ENABLED = "ENABLED";
    private static final String EUR = "EUR";
    private static final String DATE = LocalDateTime.now().format(DateTimeFormatter.ISO_DATE);
    private static final String BASIC = "BASIC";
    private static final String PRODUCT_STATUS = "CONTRACT";
    private static final String SOURCE_TYPE = "FUNNEL_BOOKING";
    private static final String ACTIVATED = "ACTIVATED";
    private static final String WEBSITE = "ES";
    private static final String RECURRING_ID = "NON_RECURRING";
    public static final long ID = 123L;


    @Override
    protected MembershipResponse getBean() {
        MembershipResponse membershipResponse = new MembershipResponse();
        membershipResponse.setActivationDate(DATE);
        membershipResponse.setAutoRenewal(ENABLED);
        membershipResponse.setBalance(BigDecimal.TEN);
        membershipResponse.setCurrencyCode(EUR);
        membershipResponse.setExpirationDate(DATE);
        membershipResponse.setTimestamp(DATE);
        membershipResponse.setId(ID);
        membershipResponse.setMemberAccount(new MemberAccountResponse());
        membershipResponse.setMemberAccountId(ID);
        membershipResponse.setMembershipType(BASIC);
        membershipResponse.setMemberStatusActions(Collections.emptyList());
        membershipResponse.setMonthsDuration(3);
        membershipResponse.setProductStatus(PRODUCT_STATUS);
        membershipResponse.setRecurringId(RECURRING_ID);
        membershipResponse.setSourceType(SOURCE_TYPE);
        membershipResponse.setStatus(ACTIVATED);
        membershipResponse.setTotalPrice(BigDecimal.TEN);
        membershipResponse.setUserCreditCardId(BigDecimal.ONE);
        membershipResponse.setWebsite(WEBSITE);
        return membershipResponse;
    }

    @Override
    protected boolean checkEquals() {
        return true;
    }

    @Override
    protected boolean checkHashCode() {
        return true;
    }

    @Test
    public void testEquals() {
        MemberAccountResponse memberAccountResponse = new MemberAccountResponse();
        memberAccountResponse.setUserId(123L);
        EqualsVerifier.forClass(MembershipResponse.class)
                .withPrefabValues(MemberAccountResponse.class, memberAccountResponse, new MemberAccountResponse())
                .suppress(Warning.NONFINAL_FIELDS)
                .usingGetClass()
                .verify();
    }
}
