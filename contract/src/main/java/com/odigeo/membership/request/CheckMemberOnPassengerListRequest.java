package com.odigeo.membership.request;

import com.odigeo.membership.request.container.TravellerParametersContainer;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class CheckMemberOnPassengerListRequest implements Serializable {

    @NotNull
    private Long userId;
    @NotNull
    private String site;
    @NotNull
    private List<TravellerParametersContainer> travellerContainerList;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public List<TravellerParametersContainer> getTravellerContainerList() {
        return travellerContainerList;
    }

    public void setTravellerContainerList(List<TravellerParametersContainer> travellerContainerList) {
        this.travellerContainerList = travellerContainerList;
    }
}
