package com.odigeo.membership.request.product.creation;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Map;

import static java.util.Objects.nonNull;

public class CreateNewMembershipRequest extends CreateMembershipRequest {

    private static final String USER_ID = "userId";
    private static final String NAME_FIELD = "name";
    private static final String LAST_NAMES = "lastNames";
    private static final String SUBSCRIPTION_PRICE = "subscriptionPrice";

    @NotNull
    private String userId;
    @NotNull
    private String name;
    @NotNull
    private String lastNames;

    private BigDecimal subscriptionPrice;

    CreateNewMembershipRequest() {
    }

    CreateNewMembershipRequest(Builder builder) {
        super(builder.getWebsite(), builder.getMonthsToRenewal(), builder.getSourceType(), builder.getMembershipType());
        this.userId = builder.getUserId();
        this.name = builder.getName();
        this.lastNames = builder.getLastNames();
        this.subscriptionPrice = builder.getSubscriptionPrice();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastNames() {
        return lastNames;
    }

    public void setLastNames(String lastNames) {
        this.lastNames = lastNames;
    }

    public BigDecimal getSubscriptionPrice() {
        return subscriptionPrice;
    }

    public void setSubscriptionPrice(BigDecimal subscriptionPrice) {
        this.subscriptionPrice = subscriptionPrice;
    }

    @Override
    protected void populateConcreteClassFields(Map<String, String> map) {
        map.put(USER_ID, userId);
        map.put(NAME_FIELD, name);
        map.put(LAST_NAMES, lastNames);
        if (nonNull(subscriptionPrice)) {
            map.put(SUBSCRIPTION_PRICE, subscriptionPrice.toString());
        }
    }

    public static class Builder {

        private String userId;
        private String name;
        private String lastNames;
        private String website;
        private int monthsToRenewal;
        private String sourceType;
        private String membershipType;
        private BigDecimal subscriptionPrice;

        String getUserId() {
            return userId;
        }

        public Builder withUserId(String userId) {
            this.userId = userId;
            return this;
        }

        String getName() {
            return name;
        }

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        String getLastNames() {
            return lastNames;
        }

        public Builder withLastNames(String lastNames) {
            this.lastNames = lastNames;
            return this;
        }

        String getWebsite() {
            return website;
        }

        public Builder withWebsite(String website) {
            this.website = website;
            return this;
        }

        int getMonthsToRenewal() {
            return monthsToRenewal;
        }

        public Builder withMonthsToRenewal(int monthsToRenewal) {
            this.monthsToRenewal = monthsToRenewal;
            return this;
        }

        String getSourceType() {
            return sourceType;
        }

        public Builder withSourceType(String sourceType) {
            this.sourceType = sourceType;
            return this;
        }

        String getMembershipType() {
            return membershipType;
        }

        public Builder withMembershipType(String membershipType) {
            this.membershipType = membershipType;
            return this;
        }

        BigDecimal getSubscriptionPrice() {
            return subscriptionPrice;
        }

        public Builder withSubscriptionPrice(BigDecimal subscriptionPrice) {
            this.subscriptionPrice = subscriptionPrice;
            return this;
        }

        public CreateNewMembershipRequest build() {
            return new CreateNewMembershipRequest(this);
        }
    }
}
