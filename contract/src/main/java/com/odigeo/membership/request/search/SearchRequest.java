package com.odigeo.membership.request.search;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.odigeo.membership.exception.MembershipInternalServerErrorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Serializable;

public class SearchRequest implements Serializable {
    private static final Logger LOGGER = LoggerFactory.getLogger(SearchRequest.class);
    protected static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    @Override
    public String toString() {
        try {
            return OBJECT_MAPPER.writeValueAsString(this);
        } catch (IOException e) {
            LOGGER.error("Impossible convert to jsonQueryParameter", e);
            throw new MembershipInternalServerErrorException(e.getMessage(), e);
        }
    }
}
