package com.odigeo.membership;

import com.odigeo.membership.exception.MembershipServiceException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jboss.resteasy.spi.validation.ValidateRequest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import static com.odigeo.membership.utils.Constants.JSON_MIME_TYPE;

@Path("/crm-decryption/v1")
@ValidateRequest
@Api(value = "CRM decryption resources", tags = "CRM Decryption v1")
public interface CRMDecryptionApi {

    @GET
    @Path("/decrypt/{token}")
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Decrypt token to name and email")
    String getCrmDecryption(@PathParam("token") String token) throws MembershipServiceException;
}
