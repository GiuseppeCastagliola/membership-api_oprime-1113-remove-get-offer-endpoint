package com.odigeo.membership.exception;


import javax.ws.rs.core.Response;

public class MembershipUnauthorizedException extends MembershipServiceException {

    public MembershipUnauthorizedException(String message) {
        this(message, null);
    }

    public MembershipUnauthorizedException(String message, Throwable cause) {
        super(Response.Status.UNAUTHORIZED, message, cause);
    }

}
