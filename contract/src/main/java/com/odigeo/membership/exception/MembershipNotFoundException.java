package com.odigeo.membership.exception;


import javax.ws.rs.core.Response;

public class MembershipNotFoundException extends MembershipServiceException {

    public MembershipNotFoundException(String message) {
        this(message, null);
    }

    public MembershipNotFoundException(String message, Throwable cause) {
        super(Response.Status.NOT_FOUND, message, cause);
    }

}
