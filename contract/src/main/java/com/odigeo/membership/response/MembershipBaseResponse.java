package com.odigeo.membership.response;

import java.math.BigDecimal;
import java.util.List;

public abstract class MembershipBaseResponse {

    private String name;
    private String lastNames;
    @Deprecated
    private long memberId;
    private long membershipId;
    private long memberAccountId;
    private String website;
    private List<MembershipWarning> warnings;
    private String autoRenewalStatus;
    private String membershipStatus;
    private String activationDate;
    private String expirationDate;
    private String recurringId;
    private String membershipType;
    private BigDecimal balance;
    private String sourceType;
    private int monthsDuration;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastNames() {
        return lastNames;
    }

    public void setLastNames(String lastNames) {
        this.lastNames = lastNames;
    }

    public long getMemberId() {
        return memberId;
    }

    public void setMemberId(long memberId) {
        this.memberId = memberId;
    }

    public long getMembershipId() {
        return membershipId;
    }

    public MembershipBaseResponse setMembershipId(long membershipId) {
        this.membershipId = membershipId;
        return this;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public List<MembershipWarning> getWarnings() {
        return warnings;
    }

    public void setWarnings(List<MembershipWarning> warnings) {
        this.warnings = warnings;
    }

    public String getAutoRenewalStatus() {
        return autoRenewalStatus;
    }

    public void setAutoRenewalStatus(String autoRenewalStatus) {
        this.autoRenewalStatus = autoRenewalStatus;
    }

    public String getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(String activationDate) {
        this.activationDate = activationDate;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getMembershipStatus() {
        return membershipStatus;
    }

    public MembershipBaseResponse setMembershipStatus(String membershipStatus) {
        this.membershipStatus = membershipStatus;
        return this;
    }

    public long getMemberAccountId() {
        return memberAccountId;
    }

    public MembershipBaseResponse setMemberAccountId(long memberAccountId) {
        this.memberAccountId = memberAccountId;
        return this;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public String getRecurringId() {
        return recurringId;
    }

    public void setRecurringId(String recurringId) {
        this.recurringId = recurringId;
    }

    public String getMembershipType() {
        return membershipType;
    }

    public MembershipBaseResponse setMembershipType(String membershipType) {
        this.membershipType = membershipType;
        return this;
    }

    public String getSourceType() {
        return sourceType;
    }

    public MembershipBaseResponse setSourceType(String sourceType) {
        this.sourceType = sourceType;
        return this;
    }

    public int getMonthsDuration() {
        return monthsDuration;
    }

    public void setMonthsDuration(int monthsDuration) {
        this.monthsDuration = monthsDuration;
    }
}
