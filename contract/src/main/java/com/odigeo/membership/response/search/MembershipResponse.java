package com.odigeo.membership.response.search;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.odigeo.membership.response.MemberStatusAction;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MembershipResponse implements Serializable {
    private long id;
    private String website;
    private String status;
    private String autoRenewal;
    private long memberAccountId;
    private String expirationDate;
    private String activationDate;
    private String timestamp;
    private BigDecimal userCreditCardId;
    private String membershipType;
    private BigDecimal balance;
    private Integer monthsDuration;
    private String productStatus;
    private BigDecimal totalPrice;
    private String currencyCode;
    private String sourceType;
    private String recurringId;
    private List<MemberStatusAction> memberStatusActions;
    private MemberAccountResponse memberAccount;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAutoRenewal() {
        return autoRenewal;
    }

    public void setAutoRenewal(String autoRenewal) {
        this.autoRenewal = autoRenewal;
    }

    public long getMemberAccountId() {
        return memberAccountId;
    }

    public void setMemberAccountId(long memberAccountId) {
        this.memberAccountId = memberAccountId;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(String activationDate) {
        this.activationDate = activationDate;
    }

    public BigDecimal getUserCreditCardId() {
        return userCreditCardId;
    }

    public void setUserCreditCardId(BigDecimal userCreditCardId) {
        this.userCreditCardId = userCreditCardId;
    }

    public String getMembershipType() {
        return membershipType;
    }

    public void setMembershipType(String membershipType) {
        this.membershipType = membershipType;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public Integer getMonthsDuration() {
        return monthsDuration;
    }

    public void setMonthsDuration(Integer monthsDuration) {
        this.monthsDuration = monthsDuration;
    }

    public String getProductStatus() {
        return productStatus;
    }

    public void setProductStatus(String productStatus) {
        this.productStatus = productStatus;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    public List<MemberStatusAction> getMemberStatusActions() {
        return memberStatusActions;
    }

    public void setMemberStatusActions(List<MemberStatusAction> memberStatusActions) {
        this.memberStatusActions = memberStatusActions;
    }

    public String getRecurringId() {
        return recurringId;
    }

    public void setRecurringId(String recurringId) {
        this.recurringId = recurringId;
    }

    public MemberAccountResponse getMemberAccount() {
        return memberAccount;
    }

    public void setMemberAccount(MemberAccountResponse memberAccount) {
        this.memberAccount = memberAccount;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MembershipResponse that = (MembershipResponse) o;
        return id == that.id && memberAccountId == that.memberAccountId
                && Objects.equals(monthsDuration, that.monthsDuration) && Objects.equals(website, that.website)
                && Objects.equals(status, that.status) && Objects.equals(autoRenewal, that.autoRenewal)
                && Objects.equals(expirationDate, that.expirationDate) && Objects.equals(activationDate, that.activationDate)
                && Objects.equals(userCreditCardId, that.userCreditCardId) && Objects.equals(membershipType, that.membershipType)
                && Objects.equals(balance, that.balance) && Objects.equals(productStatus, that.productStatus)
                && Objects.equals(totalPrice, that.totalPrice) && Objects.equals(currencyCode, that.currencyCode)
                && Objects.equals(sourceType, that.sourceType) && Objects.equals(memberStatusActions, that.memberStatusActions)
                && Objects.equals(recurringId, that.recurringId) && Objects.equals(memberAccount, that.memberAccount)
                && Objects.equals(timestamp, that.timestamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, website, status, autoRenewal, memberAccountId, expirationDate, activationDate, timestamp,
                userCreditCardId, membershipType, balance, monthsDuration, productStatus, totalPrice, currencyCode, sourceType,
                memberStatusActions, recurringId, memberAccount);
    }

}
