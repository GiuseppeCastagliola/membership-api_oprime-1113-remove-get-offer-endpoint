package com.odigeo.membership;

import com.odigeo.membership.exception.InvalidParametersException;
import com.odigeo.membership.exception.MembershipServiceException;
import com.odigeo.membership.request.postbooking.PostBookingPageInfoRequest;
import com.odigeo.membership.request.product.CreateMembershipSubscriptionRequest;
import com.odigeo.membership.response.PostBookingBatchResultResponse;
import com.odigeo.membership.response.PostBookingPageInfoResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.jboss.resteasy.spi.validation.ValidateRequest;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import static com.odigeo.membership.utils.Constants.JSON_MIME_TYPE;

@Path("/membership/v1/post-booking")
@ValidateRequest
@Api(description = "Post booking operations", tags = "Post Booking v1")
public interface PostBookingMemberService {

    @POST
    @Path("/file-service")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Create membership subscriptions by providing CSV file")
    PostBookingBatchResultResponse processPostBookingCreation(MultipartFormDataInput input) throws InvalidParametersException,
            MembershipServiceException;

    @POST
    @Path("/membership")
    @Consumes({JSON_MIME_TYPE})
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Create a membership for a user")
    Long createPostBookingMembership(@Valid CreateMembershipSubscriptionRequest createMembershipSubscriptionRequest)
            throws InvalidParametersException, MembershipServiceException;

    @POST
    @Path("/membership-pending")
    @Consumes({JSON_MIME_TYPE})
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Create a membership for a user that will be activated only when the booking will be updated\nadding the subscription product")
    Long createPostBookingPendingMembership(@Valid CreateMembershipSubscriptionRequest createMembershipSubscriptionRequest)
            throws InvalidParametersException, MembershipServiceException;

    @POST
    @Path("/get-post-booking-page-info/")
    @Consumes({JSON_MIME_TYPE})
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Decrypts the token from the post-booking page")
    PostBookingPageInfoResponse getPostBookingPageInfo(@Valid PostBookingPageInfoRequest postBookingPageInfoRequest)
            throws MembershipServiceException;
}
