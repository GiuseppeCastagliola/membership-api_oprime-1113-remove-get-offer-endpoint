package com.odigeo.membership;

import com.odigeo.membership.exception.InvalidParametersException;
import com.odigeo.membership.exception.MembershipServiceException;
import com.odigeo.membership.response.MemberSubscriptionDetails;
import com.odigeo.membership.response.MembershipAccountInfo;
import com.odigeo.membership.response.MembershipInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jboss.resteasy.spi.validation.ValidateRequest;

import javax.validation.constraints.NotNull;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import java.util.List;

import static com.odigeo.membership.utils.Constants.JSON_MIME_TYPE;

@Path("/memberUserArea/v1")
@ValidateRequest
@Api(value = "Membership User Area Operations", tags = "Membership user area v1")
public interface MemberUserArea {

    @GET
    @Path("/allSubscriptionDetails/{memberAccountId}")
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Get details of all memberships corresponding to given memberAccountId")
    List<MemberSubscriptionDetails> getAllMemberSubscriptionsDetails(@NotNull @PathParam("memberAccountId") Long memberAccountId) throws InvalidParametersException, MembershipServiceException;

    @GET
    @Path("/memberUserInfo/{membershipId}")
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Get details of given membershipId. If no booking info needed it can be explicitly avoided otherwise it will be always included by default.")
    MembershipInfo getMembershipInfo(@NotNull @PathParam("membershipId") Long membershipId, @DefaultValue ("false") @QueryParam("skipBooking") Boolean skipBooking) throws MembershipServiceException;

    @GET
    @Path("/userId/{membershipId}")
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Find userId corresponding to given membershipId")
    long getUserId(@NotNull @PathParam("membershipId") Long membershipId) throws MembershipServiceException;

    @GET
    @Path("/hasMembershipForBrand/{userId}/{brandCode}")
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Returns whether a user has any active membership for any site of the requested brand or not",
            notes = "The brand code is a two character string, for example: ED, OP or GO used for EDreams, Opodo and GoVoyage.")
    boolean userHasMembershipForBrand(@NotNull @PathParam("userId") long userId, @NotNull @PathParam("brandCode") String brandCode) throws MembershipServiceException;

    @GET
    @Path("/membershipAccountId/{membershipId}")
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Find membershipAccountId corresponding to given membershipId")
    long getMembershipAccountId(@NotNull @PathParam("membershipId") Long membershipId) throws MembershipServiceException;

    @GET
    @Path("/membershipAccount/{membershipAccountId}")
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Find membership account corresponding to given membershipAccountId")
    MembershipAccountInfo getMembershipAccount(@NotNull @PathParam("membershipAccountId") Long membershipAccountId) throws MembershipServiceException;
}
