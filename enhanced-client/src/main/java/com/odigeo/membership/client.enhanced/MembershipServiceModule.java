package com.odigeo.membership.client.enhanced;

import com.odigeo.commons.rest.ServiceNotificator;
import com.odigeo.commons.rest.configuration.ConnectionConfiguration;
import com.odigeo.commons.rest.guice.AbstractRestUtilsModule;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;
import com.odigeo.membership.MembershipService;
import com.odigeo.membership.request.auth.MembershipModuleCredentials;

public class MembershipServiceModule extends AbstractRestUtilsModule<MembershipService> {

    private static final int DEFAULT_CONNECTION_TIMEOUT_IN_MILLIS = 60 * 1000;
    private static final int DEFAULT_SOCKET_TIMEOUT_IN_MILLIS = 60 * 1000;
    private static final int DEFAUTL_POOL_SIZE = 50;

    private final ConnectionConfiguration connectionConfiguration;

    private MembershipServiceModule(ConnectionConfiguration connectionConfiguration, ServiceNotificator... serviceNotificators) {
        super(MembershipService.class, serviceNotificators);
        this.connectionConfiguration = connectionConfiguration;
    }

    @Override
    protected ServiceConfiguration<MembershipService> getServiceConfiguration(Class<MembershipService> serviceContractClass) {
        final ServiceConfiguration.Builder<MembershipService> builder = new ServiceConfiguration.Builder<MembershipService>(serviceContractClass).
                withConnectionConfiguration(connectionConfiguration);
        return builder.build();
    }

    @SuppressWarnings("PMD.AccessorClassGeneration")
    public static class Builder {
        private int connectionTimeoutInMillis;
        private int socketTimeoutInMillis;
        private int maxConcurrentConnections;
        private MembershipModuleCredentials credentials;

        public Builder() {
            connectionTimeoutInMillis = DEFAULT_CONNECTION_TIMEOUT_IN_MILLIS;
            socketTimeoutInMillis = DEFAULT_SOCKET_TIMEOUT_IN_MILLIS;
            maxConcurrentConnections = DEFAUTL_POOL_SIZE;
        }

        public Builder withConnectionTimeoutInMillis(int connectionTimeoutInMillis) {
            this.connectionTimeoutInMillis = connectionTimeoutInMillis;
            return this;
        }

        public Builder withSocketTimeoutInMillis(int socketTimeoutInMillis) {
            this.socketTimeoutInMillis = socketTimeoutInMillis;
            return this;
        }

        public Builder withMaxConcurrentConnections(int maxConcurrentConnections) {
            this.maxConcurrentConnections = maxConcurrentConnections;
            return this;
        }

        public Builder withCredentials(MembershipModuleCredentials credentials) {
            this.credentials = credentials;
            return this;
        }

        public MembershipServiceModule build(ServiceNotificator... serviceNotificators) {
            ConnectionConfiguration connectionConfiguration = new ConnectionConfiguration.Builder()
                    .connectionTimeoutInMillis(connectionTimeoutInMillis)
                    .maxConcurrentConnections(maxConcurrentConnections)
                    .socketTimeoutInMillis(socketTimeoutInMillis)
                    .credentials(credentials)
                    .build();
            return new MembershipServiceModule(connectionConfiguration, serviceNotificators);
        }

    }
}
