package com.odigeo.membership.client.enhanced;

import javax.ws.rs.core.Application;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ServiceApplication extends Application {

    private final Set<Object> singletons = new HashSet<>();

    public ServiceApplication() {
        singletons.addAll(buildRestControllers());
    }

    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }

    private List<Object> buildRestControllers() {
        return Arrays.asList(
                new MembershipController(),
                new MemberUserAreaController(),
                new PostBookingMemberController(),
                new MembershipBackOfficeController(),
                new MembershipSearchApiController(),
                new MembershipPropertiesConfigServiceController()
        );
    }
}
