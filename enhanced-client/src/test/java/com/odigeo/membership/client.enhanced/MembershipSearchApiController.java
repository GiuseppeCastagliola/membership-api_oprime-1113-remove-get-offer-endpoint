package com.odigeo.membership.client.enhanced;

import com.odigeo.membership.MembershipSearchApi;
import com.odigeo.membership.exception.MembershipServiceException;
import com.odigeo.membership.request.search.MemberAccountSearchRequest;
import com.odigeo.membership.request.search.MembershipSearchRequest;
import com.odigeo.membership.response.search.MemberAccountResponse;
import com.odigeo.membership.response.search.MembershipResponse;

import java.util.List;

public class MembershipSearchApiController implements MembershipSearchApi {
    @Override
    public MemberAccountResponse getMemberAccount(Long memberAccountId, boolean withMemberships) throws MembershipServiceException {
        return new MemberAccountResponse();
    }

    @Override
    public List<MemberAccountResponse> searchAccounts(MemberAccountSearchRequest searchRequest) throws MembershipServiceException {
        throw new UnsupportedOperationException();
    }

    @Override
    public MembershipResponse getMembership(Long membershipId) throws MembershipServiceException {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<MembershipResponse> searchMemberships(MembershipSearchRequest searchRequest) throws MembershipServiceException {
        throw new UnsupportedOperationException();
    }
}
