package com.odigeo.membership.client.enhanced;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.PostBookingMemberService;
import com.odigeo.membership.request.auth.MembershipModuleCredentials;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class PostBookingMembershipServiceModuleTest extends ModuleTest {

    public static final String TEST_N = "testN";
    public static final String TEST_P = "testP";
    public static final int CONNECTION_TIMEOUT_IN_MILLIS = 60000;
    public static final int MAX_CONCURRENT_CONNECTIONS = 100;
    private MembershipModuleCredentials credentials;
    private PostBookingMembershipServiceModule postBookingMembershipServiceModule;

    @BeforeMethod
    public void initConfigurationEngine() {
        credentials = new MembershipModuleCredentials(TEST_N, TEST_P);
        postBookingMembershipServiceModule = new PostBookingMembershipServiceModule.Builder()
                .withConnectionTimeoutInMillis(CONNECTION_TIMEOUT_IN_MILLIS)
                .withSocketTimeoutInMillis(CONNECTION_TIMEOUT_IN_MILLIS)
                .withMaxConcurrentConnections(MAX_CONCURRENT_CONNECTIONS)
                .withCredentials(new MembershipModuleCredentials(TEST_N, TEST_P))
                .build(new DoNothingServiceNotificator());
        ConfigurationEngine.init(postBookingMembershipServiceModule);
    }

    @Test
    public void testModuleConfigurationAndAvailability() {
        assertEquals(postBookingMembershipServiceModule
                .getServiceConfiguration(PostBookingMemberService.class)
                .getConnectionConfiguration().getCredentials(), credentials);
    }
}
