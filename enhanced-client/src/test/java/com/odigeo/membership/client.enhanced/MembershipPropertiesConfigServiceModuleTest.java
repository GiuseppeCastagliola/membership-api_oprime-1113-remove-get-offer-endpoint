package com.odigeo.membership.client.enhanced;


import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.MembershipPropertiesConfigService;
import com.odigeo.membership.request.auth.MembershipModuleCredentials;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class MembershipPropertiesConfigServiceModuleTest extends ModuleTest {

    public static final String TEST_N = "testN";
    public static final String TEST_P = "testP";
    public static final int CONNECTION_TIMEOUT_IN_MILLIS = 60000;
    public static final int MAX_CONCURRENT_CONNECTIONS = 100;
    private MembershipModuleCredentials credentials;
    private MembershipPropertiesConfigServiceModule propertiesConfigServiceModule;

    @BeforeMethod
    public void initConfigurationEngine() {
        credentials = new MembershipModuleCredentials(TEST_N, TEST_P);
        propertiesConfigServiceModule = new MembershipPropertiesConfigServiceModule.Builder()
                .withConnectionTimeoutInMillis(CONNECTION_TIMEOUT_IN_MILLIS)
                .withSocketTimeoutInMillis(CONNECTION_TIMEOUT_IN_MILLIS)
                .withMaxConcurrentConnections(MAX_CONCURRENT_CONNECTIONS)
                .withCredentials(new MembershipModuleCredentials(TEST_N, TEST_P))
                .build(new DoNothingServiceNotificator());
        ConfigurationEngine.init(propertiesConfigServiceModule);
    }

    @Test
    public void testModuleConfigurationAndAvailability() {
        MembershipPropertiesConfigService propertiesConfigService = ConfigurationEngine.getInstance(MembershipPropertiesConfigService.class);
        Assert.assertTrue(propertiesConfigService.enableConfigurationProperty("test"));
        Assert.assertEquals(propertiesConfigServiceModule
                .getServiceConfiguration(MembershipPropertiesConfigService.class)
                .getConnectionConfiguration().getCredentials(), credentials);
    }
}
