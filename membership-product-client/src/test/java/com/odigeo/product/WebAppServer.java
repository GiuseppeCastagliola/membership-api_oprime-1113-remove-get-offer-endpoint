package com.odigeo.product;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.WebAppContext;

import java.net.URL;

public class WebAppServer {

    public static final String RESOURCE_BASE = "/";
    public static final String CONTEXT_PATH = "/product";
    public static final String WEB_INF = "WEB-INF/";
    public static final int PORT = 18082;
    private Server server;

    public WebAppServer(String webXmlFile) {
        final WebAppContext context = new WebAppContext();
        context.setDescriptor(getWebXmlURL(webXmlFile).getFile());
        context.setResourceBase(RESOURCE_BASE);
        context.setContextPath(CONTEXT_PATH);
        context.setParentLoaderPriority(true);

        server = new Server(PORT);
        server.setHandler(context);
    }

    public void startServer() throws Exception {
        server.start();
    }

    public void stopServer() throws Exception {
        server.stop();
    }

    private URL getWebXmlURL(String webXmlFile) {
        return getClass().getClassLoader().getResource(WEB_INF + webXmlFile);
    }
}
