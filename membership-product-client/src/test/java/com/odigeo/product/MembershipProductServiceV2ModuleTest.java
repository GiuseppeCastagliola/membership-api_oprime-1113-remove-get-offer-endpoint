package com.odigeo.product;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.product.request.auth.MembershipProductModuleCredentials;
import com.odigeo.product.response.BookingApiMembershipInfo;
import com.odigeo.product.v2.exception.ProductException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public class MembershipProductServiceV2ModuleTest {

    private static final String BUILDER_WEB_XML = "builder-web.xml";
    private static final String TEST_N = "testN";
    private static final String TEST_P = "testP";
    private static final String PRODUCT_ID = "1";
    private static final int CONNECTION_TIMEOUT_IN_MILLIS = 60000;
    private static final int MAX_CONCURRENT_CONNECTIONS = 100;
    private WebAppServer webAppServer;
    private MembershipProductModuleCredentials credentials;
    private MembershipProductServiceModuleV2 membershipServiceModule;

    @BeforeClass
    public void setUp() throws Exception {
        webAppServer = new WebAppServer(BUILDER_WEB_XML);
        webAppServer.startServer();
    }

    @AfterClass
    public void setDown() throws Exception {
        webAppServer.stopServer();
    }

    @BeforeMethod
    public void initConfigurationEngine() {
        credentials = new MembershipProductModuleCredentials(TEST_N, TEST_P);
        membershipServiceModule = new MembershipProductServiceModuleV2.Builder()
                .withConnectionTimeoutInMillis(CONNECTION_TIMEOUT_IN_MILLIS)
                .withMaxConcurrentConnections(MAX_CONCURRENT_CONNECTIONS)
                .withCredentials(new MembershipProductModuleCredentials(TEST_N, TEST_P))
                .build(new DoNothingServiceNotificator());
        ConfigurationEngine.init(membershipServiceModule);
    }

    @Test
    public void testModuleConfigurationAndAvailability() throws ProductException {
        MembershipProductServiceV2 membershipService = ConfigurationEngine.getInstance(MembershipProductServiceV2.class);
        assertNotNull(membershipService.getProduct(PRODUCT_ID));
        assertEquals(membershipServiceModule
                .getServiceConfiguration(MembershipProductServiceV2.class)
                .getConnectionConfiguration().getCredentials(), credentials);
    }

    @Test
    public void testGetBookingApiProductInfo() throws ProductException {
        MembershipProductServiceV2 membershipService = ConfigurationEngine.getInstance(MembershipProductServiceV2.class);
        assertNotNull(membershipService.getBookingApiProductInfo(PRODUCT_ID));
        assertTrue(membershipService.getBookingApiProductInfo(PRODUCT_ID) instanceof BookingApiMembershipInfo);
        BookingApiMembershipInfo bookingApiMembershipInfo = membershipService.getBookingApiProductInfo(PRODUCT_ID);
        assertNotNull(bookingApiMembershipInfo);
    }
}
