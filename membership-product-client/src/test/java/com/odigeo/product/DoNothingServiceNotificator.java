package com.odigeo.product;

import com.odigeo.commons.rest.ServiceNotificator;

/**
 * Example, please not use this implementation in production.
 *
 * {@see com.odigeo.commons.rest.monitoring.dump.AutoDumpStateRegister}
 * {@see com.odigeo.commons.rest.monitoring.healthcheck.AutoHealthCheckRegister}
 */
public class DoNothingServiceNotificator implements ServiceNotificator {
    @Override
    public <Service> void notify(Class<Service> clazz) {
        //DoNothing
    }
}
