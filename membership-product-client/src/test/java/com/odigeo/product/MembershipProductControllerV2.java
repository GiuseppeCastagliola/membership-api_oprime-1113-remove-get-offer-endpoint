package com.odigeo.product;

import com.odigeo.product.response.BookingApiMembershipInfo;
import com.odigeo.product.v2.exception.ProductException;
import com.odigeo.product.v2.model.CloseProductProviderRequest;
import com.odigeo.product.v2.model.Price;
import com.odigeo.product.v2.model.Product;
import com.odigeo.product.v2.model.payment.PaymentEntity;
import com.odigeo.product.v2.model.responses.CloseProviderProductResponse;
import com.odigeo.product.v2.model.responses.SetPaymentDetailsResponse;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;


public class MembershipProductControllerV2 implements MembershipProductServiceV2 {

    private static final String MESSAGE = "Not needed. This is just a test Stub";

    @Override
    public Product getProduct(String productId) throws ProductException {
        Product product = new Product();
        Price price = new Price();
        price.setAmount(BigDecimal.ZERO);
        price.setCurrency(Currency.getInstance("EUR"));
        return product;
    }

    @Override
    public Product getProductDetails(String productId) throws ProductException {
        throw new UnsupportedOperationException(MESSAGE);
    }

    @Override
    public SetPaymentDetailsResponse setPaymentDetails(String s, long l, PaymentEntity s1) {
        throw new UnsupportedOperationException(MESSAGE);
    }

    @Override
    public CloseProviderProductResponse closeProviderProduct(String s, String s1, CloseProductProviderRequest closeProductProviderRequest) throws ProductException {
        throw new UnsupportedOperationException(MESSAGE);
    }

    @Override
    public void rollbackTransactionProduct(String s, String s1) throws ProductException {
        throw new UnsupportedOperationException(MESSAGE);
    }

    @Override
    public void cancelTransactionProduct(String s, String s1) throws ProductException {
        throw new UnsupportedOperationException(MESSAGE);
    }

    @Override
    public void commitTransactionProduct(String s, String s1) throws ProductException {
        throw new UnsupportedOperationException(MESSAGE);
    }

    @Override
    public void suspendTransactionProduct(String s, String s1) throws ProductException {
        throw new UnsupportedOperationException(MESSAGE);
    }

    @Override
    public void resumeTransactionProduct(String s, String s1) throws ProductException {
        throw new UnsupportedOperationException(MESSAGE);
    }

    @Override
    public void saveProduct(String s, boolean partial) throws ProductException {
        throw new UnsupportedOperationException(MESSAGE);
    }

    @Override
    public BookingApiMembershipInfo getBookingApiProductInfo(String productId) throws ProductException {
        BookingApiMembershipInfo bookingApiMembershipInfo = new BookingApiMembershipInfo();
        bookingApiMembershipInfo.setMembershipId(Long.valueOf(productId));
        bookingApiMembershipInfo.setRenewal(Boolean.TRUE);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, 1);
        Date nextYear = cal.getTime();
        bookingApiMembershipInfo.setRenewalDate(nextYear);
        return bookingApiMembershipInfo;
    }
}
