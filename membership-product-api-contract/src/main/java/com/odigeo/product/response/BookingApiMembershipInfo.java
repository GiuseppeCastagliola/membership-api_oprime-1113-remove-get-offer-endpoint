package com.odigeo.product.response;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.math.BigDecimal;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BookingApiMembershipInfo {

    private long membershipId;
    private boolean isRenewal;
    private Date renewalDate;
    private BigDecimal price;
    private String currencyCode;

    public long getMembershipId() {
        return membershipId;
    }

    public void setMembershipId(long membershipId) {
        this.membershipId = membershipId;
    }

    public boolean isRenewal() {
        return isRenewal;
    }

    public void setRenewal(boolean renewal) {
        isRenewal = renewal;
    }

    public Date getRenewalDate() {
        return renewalDate != null ? (Date) renewalDate.clone() : null;
    }

    public void setRenewalDate(Date renewalDate) {
        this.renewalDate = renewalDate != null ? (Date) renewalDate.clone() : null;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }
}
