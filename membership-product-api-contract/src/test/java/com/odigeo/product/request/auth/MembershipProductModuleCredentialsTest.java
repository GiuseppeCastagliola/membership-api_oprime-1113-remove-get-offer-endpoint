package com.odigeo.product.request.auth;

import org.apache.http.auth.Credentials;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.security.Principal;

public class MembershipProductModuleCredentialsTest {

    public static final String TEST_P = "testP";
    public static final String TEST_N = "testN";
    MembershipProductModuleCredentials credentials;

    @BeforeMethod
    public void setUp() {
        credentials = new MembershipProductModuleCredentials(TEST_N, TEST_P);
    }

    @Test
    public void testPassword() {
        Assert.assertNotNull(credentials.getPassword());
    }

    @Test
    public void testName() {
        Assert.assertNotNull(credentials.getUserPrincipal().getName());
    }

    @Test
    public void testToString() {
        Assert.assertTrue(credentials.toString().contains(credentials.getPassword()));
        Assert.assertTrue(credentials.toString().contains(credentials.getUserPrincipal().getName()));
    }

    @Test
    public void testEqualWhenMeaningfulEquality() {
        MembershipProductModuleCredentials otherCredentials = new MembershipProductModuleCredentials(TEST_N, TEST_P);
        Assert.assertEquals(credentials, otherCredentials);
    }

    @Test
    public void testEqualWhenIsTheSameObject() {
        Assert.assertEquals(credentials, credentials);
    }

    @Test
    public void testEqualWhenNotEqual() {
        MembershipProductModuleCredentials differentCredentials = new MembershipProductModuleCredentials("other", "test");
        Assert.assertNotEquals(credentials, differentCredentials);
    }

    @Test
    public void testEqualWhenNullObject() {
        Assert.assertNotEquals(credentials, null);
    }

    @Test
    public void testEqualWhenEmptyObject() {
        Assert.assertNotEquals(new Credentials() {
            @Override
            public Principal getUserPrincipal() {
                return null;
            }

            @Override
            public String getPassword() {
                return null;
            }
        }, credentials);
    }
}
