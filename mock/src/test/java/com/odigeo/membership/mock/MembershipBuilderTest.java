package com.odigeo.membership.mock;

import com.odigeo.membership.response.Membership;
import com.odigeo.membership.response.MembershipWarning;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

public class MembershipBuilderTest {

    @Test
    public void testBuild() {
        Membership mb = new MembershipBuilder(new Random())
                .build();
        assertNull(mb);
    }

    @Test
    public void testLastNameNotNull() {
        String name = "NAME";
        Membership mb = new MembershipBuilder(new Random())
                .setName(name)
                .build();
        assertNotNull(mb.getLastNames());
        assertEquals(mb.getName(), name);
    }

    @Test
    public void testNameNotNull() {
        String lastNames = "LAST_NAME";
        Membership mb = new MembershipBuilder(new Random())
                .setLastNames(lastNames)
                .build();
        assertNotNull(mb.getName());
        assertEquals(mb.getLastNames(), lastNames);
    }

    @Test
    public void testMembershipTypeNotNull() {
        String membershipType = "BASIC";
        Membership mb = new MembershipBuilder(new Random())
                .setMembershipType(membershipType)
                .build();
        assertNotNull(mb.getMembershipType());
        assertEquals(mb.getMembershipType(), membershipType);
    }

    @Test
    public void testStatusTypeNotNull() {
        String sourceType = "FUNNEL_BOOKING";
        Membership mb = new MembershipBuilder(new Random())
                .setSourceType(sourceType)
                .build();
        assertNotNull(mb.getSourceType());
        assertEquals(mb.getSourceType(), sourceType);
    }

    @Test
    public void testNotNull() {
        String name = "NAME";
        String lastNames = "LAST_NAME";
        long memberId = 77;
        String website = "ES";
        String autoRenewalStatus = "ENABLED";
        String membershipType = "BASIC";
        String sourceType = "FUNNEL_BOOKING";
        Membership mb = new MembershipBuilder(new Random())
                .setLastNames(lastNames)
                .setName(name)
                .setMemberId(memberId)
                .setWebsite(website)
                .setAutoRenewalStatus(autoRenewalStatus)
                .setMembershipType(membershipType)
                .setSourceType(sourceType)
                .build();
        assertNotNull(mb.getName());
        assertNotNull(mb.getLastNames());
        assertNotNull(mb.getWebsite());
        assertNotNull(mb.getWarnings());
        assertNotNull(mb.getAutoRenewalStatus());
        assertNotNull(mb.getMembershipType());
        assertNotNull(mb.getExpirationDate());
        assertNotNull(mb.getActivationDate());
        assertNotNull(mb.getSourceType());
        assertEquals(mb.getLastNames(), lastNames);
        assertEquals(mb.getName(), name);
        assertEquals(mb.getMemberId(), memberId);
        assertEquals(mb.getWebsite(), website);
        assertEquals(mb.getAutoRenewalStatus(), autoRenewalStatus);
        assertEquals(mb.getMembershipType(), membershipType);
        assertEquals(mb.getSourceType(), sourceType);
    }

    @Test
    public void testNotNullIfOnlyIdSet() {
        Membership mb = new MembershipBuilder(new Random())
                .setMembershipId(1)
                .build();
        assertNotNull(mb.getName());
        assertNotNull(mb.getLastNames());
        assertNotNull(mb.getWebsite());
        assertNotNull(mb.getWarnings());
        assertNotNull(mb.getAutoRenewalStatus());
        assertNotNull(mb.getExpirationDate());
        assertNotNull(mb.getActivationDate());
        assertNotNull(mb.getMembershipType());
        assertNotNull(mb.getSourceType());
    }

    @Test
    public void testNotZeroIfOnlyIdSet() {
        Membership mb = new MembershipBuilder(new Random())
                .setMembershipId(1)
                .build();
        assertNotEquals(mb.getMemberId(), 0L);
        assertNotEquals(mb.getMemberAccountId(), 0L);
        assertNotEquals(mb.getMembershipId(), 0L);
    }

    @Test
    public void testSetWarnings() {
        List<MembershipWarning> warnings = new ArrayList<MembershipWarning>();
        warnings.add(MembershipWarning.PERKS_USAGE_EXCEEDED);
        Membership mb = new MembershipBuilder(new Random())
                .setWarnings(warnings)
                .build();
        assertEquals(mb.getWarnings().size(), 1);
        assertTrue(mb.getWarnings().contains(MembershipWarning.PERKS_USAGE_EXCEEDED));
    }
}
