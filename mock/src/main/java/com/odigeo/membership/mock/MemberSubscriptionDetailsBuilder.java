package com.odigeo.membership.mock;

import com.odigeo.commons.test.random.Picker;
import com.odigeo.commons.test.random.StringPicker;
import com.odigeo.membership.response.CreditCard;
import com.odigeo.membership.response.MemberSubscriptionDetails;
import com.odigeo.membership.response.Money;
import com.odigeo.membership.response.PrimeBookingInformation;
import com.odigeo.membership.util.PickerUtils;

import java.util.Collections;
import java.util.List;
import java.util.Random;

public class MemberSubscriptionDetailsBuilder {

    private final StringPicker stringPicker;
    private final Picker picker;
    private Long membershipId;
    private Money totalSavings;
    private List<PrimeBookingInformation> primeBookingsInfo;
    private CreditCard subscriptionPaymentMethod;
    private String subscriptionPaymentMethodType;

    public MemberSubscriptionDetailsBuilder(Random random) {
        this.picker = new Picker(random);
        this.stringPicker = new StringPicker(random);
    }

    public MemberSubscriptionDetails build() {
        return fillMissingInfo().buildMemberSubscriptionDetails();
    }

    private MemberSubscriptionDetails buildMemberSubscriptionDetails() {
        MemberSubscriptionDetails memberSubscriptionDetails = new MemberSubscriptionDetails();
        memberSubscriptionDetails.setMembershipId(this.membershipId);
        memberSubscriptionDetails.setPrimeBookingsInfo(this.primeBookingsInfo);
        memberSubscriptionDetails.setTotalSavings(this.totalSavings);
        memberSubscriptionDetails.setSubscriptionPaymentMethod(this.subscriptionPaymentMethod);
        memberSubscriptionDetails.setSubscriptionPaymentMethodType(this.subscriptionPaymentMethodType);
        return memberSubscriptionDetails;
    }

    private MemberSubscriptionDetailsBuilder fillMissingInfo() {
        if (membershipId == null) {
            membershipId = picker.pickLong(1L, Long.MAX_VALUE);
        }
        if (totalSavings == null) {
            MoneyBuilder moneyBuilder = new MoneyBuilder(new Random());
            this.totalSavings = moneyBuilder.build();
        }
        if (primeBookingsInfo == null) {
            PrimeBookingInformation primeBookingInformation = new PrimeBookingInformationBuilder(new Random()).build();
            primeBookingsInfo = Collections.singletonList(primeBookingInformation);
        }
        if (subscriptionPaymentMethod == null) {
            this.subscriptionPaymentMethod = new CreditCardBuilder(new Random()).setId(1L).build();
        }
        if (subscriptionPaymentMethodType == null) {
            this.subscriptionPaymentMethodType = PickerUtils.pickCreditCardType(picker);
        }
        return this;
    }

    public Long getMembershipId() {
        return membershipId;
    }

    public MemberSubscriptionDetailsBuilder setMembershipId(Long membershipId) {
        this.membershipId = membershipId;
        return this;
    }

    public Money getTotalSavings() {
        return totalSavings;
    }

    public MemberSubscriptionDetailsBuilder setTotalSavings(Money totalSavings) {
        this.totalSavings = totalSavings;
        return this;
    }

    public List<PrimeBookingInformation> getPrimeBookingsInfo() {
        return primeBookingsInfo;
    }

    public MemberSubscriptionDetailsBuilder setPrimeBookingsInfo(List<PrimeBookingInformation> primeBookingsInfo) {
        this.primeBookingsInfo = primeBookingsInfo;
        return this;
    }

    public StringPicker getStringPicker() {
        return stringPicker;
    }

    public Picker getPicker() {
        return picker;
    }

    public CreditCard getSubscriptionPaymentMethod() {
        return subscriptionPaymentMethod;
    }

    public MemberSubscriptionDetailsBuilder setSubscriptionPaymentMethod(CreditCard subscriptionPaymentMethod) {
        this.subscriptionPaymentMethod = subscriptionPaymentMethod;
        return this;
    }

    public String getSubscriptionPaymentMethodType() {
        return subscriptionPaymentMethodType;
    }

    public MemberSubscriptionDetailsBuilder setSubscriptionPaymentMethodType(String subscriptionPaymentMethodType) {
        this.subscriptionPaymentMethodType = subscriptionPaymentMethodType;
        return this;
    }
}
